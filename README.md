# Prácticas Programación Hardware 2020/2021
### Jorge Martinez de la chica - jmc00133</br>Alberto Rodríguez Torres - art00027
Se incluyen drivers básicos para trabajar con la arquiteectura de drivers de Linux y experimentar con ellos y además, un bootloader x86_64 básico compatible con QEMU y hardware real.

-------------------

## DownscaleDriver 
Este es el driver que se cargará en el sistema, creará dos dispositivos los cuales, uno se encargará de hacer downscaling de imagenes en formato en crudo a 1/4 de su resolución y el otro dispositivo convertirá imagenes a escala de grises, el formato es el siguiente (anchura altura datos datos datos ...) **importante colocar espacios entre valores y finalizar con uno**.
El dispositivo de downscale /dev/DSDriver0: recibe la imagen por canales, debiendo recibir anchura y altura por cada canal de color y este debe tener una profundidad de color máxima de 8 bits (0-254)

* Se puede escribir datos a este dispositivo por consola (cat, por ejemplo) o mediante llamadas directas utilizando librerias del sistema.
* Alternativamente se pueden escribir estos datos mediante una función de IOCTL que permite escribir directamente valores númericos evitando hacer la conversión de tipos y acelerando el procesado.
* Al leer el driver mediante consola o llamada directa y devolverá el canal a 1/4 de su tamaño.

El dispositivo de grises /dev/DSDriver1: recibe anchura y altura y todos los canales de color en RGB con una profundida de color máxima de 8 bits.

* Se puede escribir datos a este dispositivo por consola (cat, por ejemplo) o mediante llamadas directas utilizando librerias del sistema.
* Alternativamente se pueden escribir estos datos mediante una función de IOCTL que permite escribir directamente valores númericos evitando hacer la conversión de tipos y acelerando el procesado.
* Al leer el driver mediante consola o llamada directa y devolverá la imagen con sus canales de colores combinados en uno (escala de grises).

**NOTA:** hemos encotrado problemas a la hora de enviar cadenas de texto grandes al driver utilizando cat, aunque la opción esté presente no es recomendable escribir imágenes utilizando la consola, en su lugar realizar llamadas directas.

## TestDriver
Se incluye un programa escrito en C++ que implementa la descompresión de imágenes en jpeg y envia el array de píxeles al driver, recomponiendo despues la imagen en un fichero. El programa realizará esta operacion dos veces, una vez mediante write y otra llamando a ioctl y graba los tiempos de procesado del driver.

Repetirá esta misma operación a continuación para convertir la misma imagen a escala de grises.

Uso: testDriver ruta/a/imagen.jpeg

---------------------

## Bootloader
Hemos escrito un bootloader bastante simple, es capaz de arrancar en máquinas emuladas en QEMU. El bootloader se inicia con la máquina en modo real y escribe unos mensajes en la pantalla para informar al usuario de que está sucediendo, acto seguido cargará desde disco la segunda etapa del bootloader, que se encargará de pasar a modo protegido i386 32 bits y donde encontramos la interfaz principal, tenemos un prompt de comandos donde podemos utilizar los siguientes comandos:

* **ECHO:** Mostrará por pantalla el mensaje tecleado a continuación.
* **HELP:** Mostrará los comandos disponibles.
* **LONG:** Incia el procedimiento necesario para pasar a modo long x86_64 de 64 bits, nos mostrará un mensaje informativo por pantalla informandonos de esto, volverá a poner el prompt de nuevo (si el sistema no es compatible, volverá al prompt en modo protegido).
* **CLEAR:** Limpia la pantalla e imprime el promt de nuevo.
* **EXIT:** Sale de modos long y protegido y llama a la interrupción BIOS para apagar correctamente la máquina (sin implementar).

Cabe destacar que tanto en modo long como en el modo protegido todas las instrucciones funcionan exactamente igual que en modo protegido. El modo real sólo sirve para pasar a modo protegido y no realiza ninguna función más, emulando el comportamiento de un bootloader que ejecuta un kernel de sistema real. Se intentó implementar un kernel escrito en Rust, pero no hemos conseguido hacer que nuestro bootloader sea capaz de llamar al main de rust, utilizando rustboot, un bootloader generico de rust, si es posible ejecutarlo. Tambien intentamos concatenar el código objeto de rust, con y sin librerias con nuestro código ensamblado utilizando un enlazador, pero tampoco surtió efecto, el código sin linrerias no enlazaba correctamente y las liberias estandar de Rust pesaban 600MB, más de lo que la interrupción de BIOS es capaz de cargar de una sola vez y más de lo que el modo real puede direccionar sin entrar en problemas mayores.

### Utilización
Para ejecutar el bootloader se puede ejecutar con QEMU usando un sistema i386 o x86_64 introduciendo como imagen de disquete el siguiente fichero Bootloader/boot.img tambien es posible obtener una directamente del código asm utilizando el script make.bat en windows o con el siguiente comando en Linux

`
$nasm boot.asm -f bin -o boot.img
` 

NASM es la única dependencia necesaria ya que utilizando DD puedes utilizar hardware real para probar el boot.img desde una unidad flash USB.