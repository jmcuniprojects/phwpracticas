@echo off
del boot.img
nasm.exe boot.asm -f bin -o boot.img
SET dir=%cd%
cd C:\Program Files\qemu
qemu-system-x86_64.exe -drive file=%dir%\boot.img,format=raw,index=0,if=floppy
cd %dir%