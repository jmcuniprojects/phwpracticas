;; Primero definimos la tabla de descripción de segmento

[bits 16]

gdt_start:
gdt_null:
	dd 0x0
	dd 0x0

;; Kernel Code Segment
gdt_kernel_code:
	dw 0xFFFF
	dw 0x0
	db 0x0
	db 10011010b
	db 11001111b
	db 0x0

;; Kernel Data Segment
gdt_kernel_data:
	dw 0xFFFF
	dw 0x0
	db 0x0
	db 10010010b
	db 11001111b
	db 0x0

;; Userland Code Segment
gdt_userland_code:
	dw 0xFFFF
	dw 0x0
	db 0x0
	db 11111010b
	db 11001111b
	db 0x0

;; Userland Data Segment
gdt_userland_data:
	dw 0xFFFF
	dw 0x0
	db 0x0
	db 11110010b
	db 11001111b
	db 0x0

gdt_end:
gdt_descriptor:
	dw gdt_end - gdt_start - 1
	dd gdt_start

CODE_SEG equ gdt_kernel_code - gdt_start
DATA_SEG equ gdt_kernel_data - gdt_start

cambiar_pm:
    cli

    lgdt [gdt_descriptor] ;; Cargamos la tabla de descripción de segmento


    ;; Ahora debemos activar el modo protegido activado el bit en  CR0
    mov eax, CR0
    or eax, 0x1
    mov cr0, eax

    ;; A partir de este  momento estamos en modo protegido, vamos a saltar a una región más o menos lejana de memoria con código de 32 bits

    call CODE_SEG:iniciar_pm
    ret

[bits 32]

iniciar_pm:
    ;; Actualiar TODOS los registros de segmento
   	mov ax, DATA_SEG
	mov ds, ax
	mov ss, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

    ;; Actualizar punteros de base y pila
    mov ebp, 0x90000
	mov esp, ebp
    call pm_main
    ret


pm_row: dd 0h
pm_col: dd 0h
print_char_pm:
    pusha
    mov ebx, 0xb8000     ; Ubicación de la memoria de vídeo
    add ebx, [pm_col]
    mov ah, 0x0F         ; Propiedades del carácter

    charloop
    mov al, [bp]         ; Copiamos a AL el caracter a imprimir
    mov [ebx], ax        ; Copiamos el caracter a memoria
    add ebx, 2           ; Cogemos la dirección de memoria del siguiente carácter
    inc bp               ; Incrementamos el offset del string
    loop charloop         ; Si hemos terminado el string continuamos

    mov eax, [pm_col]
    add eax, 160
    mov [pm_col], eax
    mov eax, [pm_row]
    inc eax
    mov [pm_row], eax
    call updateCursor
    popa
    ret

print_byte_pm:
    pusha
    mov ebx, 0xb8000     ; Ubicación de la memoria de vídeo
    add ebx, [pm_col]
    mov ah, 0x0F         ; Propiedades del carácter

    mov [ebx], ax        ; Copiamos el caracter a memoria
    add ebx, 2           ; Cogemos la dirección de memoria del siguiente carácter
    inc bp               ; Incrementamos el offset del string

    mov eax, [pm_col]
    add eax, 2
    mov [pm_col], eax
    xor edx,edx
    mov ecx, 160
    div ecx
    cmp edx, 0
    jne .finish

    mov eax, [pm_row]
    inc eax
    mov [pm_row], eax
    cmp eax, 25
    jne .finish
    call clear_screen_pm
  .finish:
    call updateCursor
    popa
    ret

print_new_line:
    pusha
    mov eax, [pm_row]
    inc eax
    mov [pm_row], eax
    cmp eax, 25
    jne .continue
    call clear_screen_pm
    xor eax,eax
  .continue:
    imul eax, 160
    mov [pm_col], eax
    call updateCursor
    popa
    ret

print_delimiter:
    pusha
    mov al, [delimitador]
    mov ecx, 1
    call print_byte_pm
    popa
    ret

delete_byte:
    pusha
    mov eax, [pm_col]
    cmp eax, 2
    je .finish
    xor edx,edx
    mov ecx, 160
    div ecx
    cmp edx, 0
    jne .continue
    mov edx, [pm_row]
    dec edx
    mov [pm_row], edx
  .continue:
    mov eax, [pm_col]
    dec eax
    dec eax
    mov [pm_col], eax
    mov al, ' '
    mov ecx, 1
    call print_byte_pm
    mov eax, [pm_col]
    dec eax
    dec eax
    mov [pm_col], eax
    call updateCursor
  .finish:
    popa
    ret

clear_screen_pm:
	pusha
	cld
	mov	edi, 0xb8000    ; puntero de la dirección de memoria de video
	mov	ecx, 2000        ; Numero de caracteres a limpiar
	mov	ah, 0x0F        ; Propiedades del caracter
	mov	al, ' '	        ; Caracter blanco o espacio
	rep	stosw           ; bucle

    ;; Limpiamos el contador de filas para escribir
    xor ebx, ebx
    mov [pm_row], ebx
    mov [pm_col], ebx
    popa
	ret

updateCursor:
    pusha
    mov dl, 80
    mov ecx, 2
    mov eax, [pm_col]
    xor edx,edx
    div ecx
    mov ebx, eax
    mov eax, [pm_row]
    mul dl
    add bx, ax

    mov dx, 0x03D4
    mov al, 0x0F
    out dx, al

    inc dl
    mov al, bl
    out dx, al

    dec dl
    mov al, 0x0E
    out dx, al

    inc dl
    mov al, bh
    out dx, al
    popa
    ret


msg7: db "ERROR: OPERACION NO SOPORTADA"
msg7len equ $ - msg7
salir_pm:
    mov bp, msg7
    mov ecx, msg7len
    call print_char_pm
    ret