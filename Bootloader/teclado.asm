[bits 32]
Keyboard_input:
  .input:
    in al, 64h
    test al, 1
    jz .input
    test al, 0x20
    jnz .input

    in al, 60h
    mov ah, al

    in al, 61h
    or al, 80h
    out 61h, al
    and al, 7Fh
    out 61h, al

    mov al, ah
    test al, 80h
    jnz .input

keypress:
    jmp convert
  keyconverted:
    mov ecx, 1
    call print_byte_pm
    mov cl, 1
    jmp Keyboard_input

convert:
    mov cl, 2
    movzx	edi,cl
    mov esi, letter_table
    .LOOP:
      mov dh, [esi]
      cmp al, dh
      je .conv
      cmp dh, 0         ;¿Final?
      je .special
      add esi, edi
      jmp .LOOP
    .conv:
      mov cl, 1
      movzx edi,cl
      add esi, edi
      mov al, [esi]
      jmp keyconverted
    .special:
      cmp al, 1Ch
      je .new_line
      cmp al, 0Eh
      jne Keyboard_input
      call delete_byte
      jmp Keyboard_input
    .new_line:
      call print_new_line
      jmp Key_out

Buffer_text: db 20 dup ("$")

buffer_order:
    pusha
    mov ebx, 0xb8000
    mov esi, [pm_row]
    dec esi
    imul esi, 160
    inc esi
    inc esi
    add ebx, esi
    mov cx, 20
    mov edi, Buffer_text
    cld
  .reading:
    mov al, [ebx]
    stosb
    add ebx, 2
    loop .reading

    xor ebx, ebx
  .checking:
    mov esi, order
    add esi, ebx
    mov edi, Buffer_text
    mov cx, 5
    cld
    rep cmpsb
    jne .not_order
    cmp ebx, 0
    je .long
    cmp ebx, 5
    je .clear
    cmp ebx, 10
    je .exit
    cmp ebx, 15
    je .echo
    cmp ebx, 20
    je .help
    popa
    jmp read_loop
  .not_order:
    add ebx, 5
    cmp ebx, 25
    jne .checking
    jmp .end
  .long:
    popa
    jmp Long_Mode
  .clear:
    call clear_screen_pm
    jmp .end
  .exit:
    call salir_pm
    jmp read_loop
  .echo:
    push bp
    push cx
    mov bp, Buffer_text
    add bp, 5
    mov cx, 15
    call print_char_pm
    pop bp
    pop cx
    jmp read_loop
  .help:
    mov bp, commands
    mov ecx, commandslen
    call print_char_pm
  .end:
    popa
    jmp read_loop

letter_table:
    db 10h,	  'Q'
    db 11h,	  'W'
    db 12h,	  'E'
    db 13h,	  'R'
    db 14h,	  'T'
    db 15h,	  'Y'
    db 16h,	  'U'
    db 17h,	  'I'
    db 18h,	  'O'
    db 19h,	  'P'
    db 1Eh,	  'A'
    db 1Fh,	  'S'
    db 20h,	  'D'
    db 21h,	  'F'
    db 22h,	  'G'
    db 23h,	  'H'
    db 24h,	  'J'
    db 25h,	  'K'
    db 26h,	  'L'
    db 2Ch,	  'Z'
    db 2Dh,	  'X'
    db 2Eh,	  'C'
    db 2Fh,	  'V'
    db 30h,	  'B'
    db 31h,	  'N'
    db 32h,	  'M'
    db 02h,	  '1'
    db 03h,	  '2'
    db 04h,	  '3'
    db 05h,	  '4'
    db 06h,	  '5'
    db 07h,	  '6'
    db 08h,	  '7'
    db 09h,	  '8'
    db 0Ah,	  '9'
    db 0Bh,	  '0'
    db 39h,    ' ';Espacio
    db 0h

order:
    db "LONG "
    db "CLEAR"
    db "EXIT "
    db "ECHO "
    db "HELP "

commands: db "LISTA DE COMANDOS: HELP ECHO CLEAR LONG EXIT"
commandslen equ $ - commands