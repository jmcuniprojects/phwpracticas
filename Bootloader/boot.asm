org 07c00h
main:
    jmp start  ;goto main
    nop

disk_read:
	;; store all register values
	pusha
	push dx

	;; prepare data for reading the disk
	;; al = number of sectors to read (1 - 128)
	;; ch = track/cylinder number
	;; dh = head number
	;; cl = sector number
	mov ah, 0x02
	mov al, dh
	xor ch, ch
	xor dh, dh

	int 0x13

	;; in case of read error
	;; show the message about it
	jc disk_read_error

	;; check if we read expected count of sectors
	;; if not, show the message with error
	pop dx
	cmp dh, al
	jne disk_read_error

	;; restore register values and ret
    popa
	ret

disk_read_error:
    mov bp, error
    mov cx, errorlen
    call write_string
	jmp $

shutdown:
    sti
    mov ax, 0x1000
    mov ax, ss
    mov sp, 0xf000
    mov ax, 0x5307
    mov bx, 0x0001
    mov cx, 0x0003
    int 0x15

    ret

msg: db 'CARGANDO SEGUNDO BOOTLOADER...'
msglen equ $ - msg

error: db "ERROR AL LEER DE DISCO"
errorlen equ $ - error

msg3: db "HAS LLEGADO AL SEGUNDO BOOTLOADER"
msg3len equ $ - msg3

msg4: db 'CAMBIANDO CPU A MODO PROTEGIDO'
msg4len equ $ - msg4

msg5: db 'YA ESTAS EN MODO LONG'
msg5len equ $ - msg5

msg6: db "ESTAS EN MODO LONG, NICE"
msg6len equ $ - msg6

delimitador: db ">"

err1: db "ERROR: CPU NO SOPORTADA, ES NECESARIO UN SISTEMA DE 64 BITS"
err1len equ $ - err1

KERNEL_MAIN equ 0x1000

write_string:
    pusha                   ; Guardamos los valores de los registros
    xor ax, ax              ; Ponemos ax a 0 porque es es un registro de 16 bits y necesitamos dos registros
    mov es, ax              ; Metemos 0 en es para indicar desplazamiento absoluto 0:bp
    mov ah, 13h             ; Indicamos que vamos a imprimir strings
    mov al, 1               ; Actualizamos cursor siguiendo el string
    xor bh, bh              ; Asignamos pagina
    mov bl, 0Fh             ; Color de texto
    mov dh, [row]           ; Fila
    xor dl, dl              ; Columna
    int 10h                 ; Llamamos la interupción

    inc dh                  ; Incrementamos el contador de filas
    mov [row], dh           ; Guardamos la fila para el siguiente string
.out popa                   ; Devolvemos los registros
    ret

disk: db 0x0
row: dw 0x0

clear_screen:
    pusha
    xor bl,bl
    mov [row],bl
    mov al, 03h; configuramos el modo grafico texto
    mov ah, 00h; este es el codigo de interrupcion
    int 10h; llamamos la interrucion
    popa
    ret

start:
    cli						; desactivamos int
    mov [disk],dl
    mov ax,cs				; configurar el registro de segmento
    mov ds,ax				; escribir el valor correcto en el registro ds
    mov es,ax				; lo mismo para el registro es
    mov ss,ax				; lo mismo para el registro ss
    mov bp,7c00h            ; configurar el puntero base de la pila
    mov sp,7c00h			; configurar el puntero de la pila
    sti						; reactivamos las interrupciones

    call clear_screen
    mov bp, msg
    mov cx, msglen
    call write_string

    mov dh, 0x4
    mov cl, 0x02
    mov dl, [disk]
    mov bx, Second
    call disk_read

    call Second ; salta a donde comienza el segundo bootloader
    call shutdown

    times 510 - ($-$$) db 0
    dw 0xAA55
%include "second.asm"