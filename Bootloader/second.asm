[bits 16]

Second:
    jmp main_second

main_second:
    call clear_screen
    mov bp, msg3
    mov cx, msg3len
    call write_string
    ;;call carga_kernel

    mov bp, msg4
    mov cx, msg4len
    call write_string
    call clear_screen
    call cambiar_pm
    call shutdown
    ret

carga_kernel:
    pusha
    xor ax, ax
    mov es, ax
    mov bx, 0x1000
    mov cx, 0xA
    bucle
    pusha
    mov dh, 0x80
    mov cl, 0x04
    mov dl, [disk]
    call disk_read
    popa
    inc bx
    mov ax, es
    inc ax
    mov es, ax
    loop bucle
    popa
    ret

[bits 32]

pm_main:
  read_loop:
    call print_delimiter
    jmp Keyboard_input
  Key_out:
    jmp buffer_order
  Long_Mode:
    mov al, [long_bit]
    cmp al, 0
    jne .noup
    call detectar_long
  .noup:
    mov bp, msg5
    mov ecx, msg5len
    call print_char_pm
    jmp read_loop
    ;;ret

[bits 64]

lm_main:
    mov al, 1
    mov [long_bit], al
    call clear_screen_pm
    mov bp, msg6
    mov ecx, msg6len
    call print_char_pm
    call print_delimiter
    jmp Keyboard_input
    jmp $

long_bit: db 0x0

%include "modo_protegido.asm"
%include "modo_long.asm"
%include "teclado.asm"
times 2048 - ($-$$) db 0