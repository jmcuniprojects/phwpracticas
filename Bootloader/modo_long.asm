[bits 32]

;; gran parte del código de cambio a modo protegido proviene de internet
;; para comprobar si el pc es compatible con modo long, debemos comprobar si soporta CPUID, para ello intentamos invertir el bit de ID

detectar_long:
    pushfd              ;; Nos guardamos el estado de los flags
    pop eax             ;; y los vaciamos en EAX

    mov ecx, eax        ;; lo metemos en ECX para comparar luego

    xor eax, 1 << 21    ;; Invertimos el bit de ID


    push eax            ;; Copiamos EAX a los flags pasando por la pila
    popfd

    pushf               ;; y nos los traemos a AEX otra vez
    pop eax

    push ecx            ;; restauramos los antiguos flags de ECX
    popfd

    ;; Comparamos EAX y ECX
    ;; Si son iguales significa que no se ha podido invertir el bit, no tenemos 64 bits
    xor eax, ecx
    jz CPU_not_supported

    ;; En caso de soportar CPUID, vamos a ver si el procesador puede utilizar instrucciones extendidas
    mov eax, 0x80000000
    cpuid
    cmp eax, 0x80000001
    jb CPU_not_supported

    ;; Ahora SI podemos comprobar si soporta modo long o no
    mov eax, 0x80000001
    cpuid
    test edx, 1 << 29
    jz CPU_not_supported

    ;; Si pasamos TODAS las comprobaciones, activamos el modo long
    call cambiar_lm
    ret

CPU_not_supported:
    mov bp, err1
    mov cx, err1len
    call print_char_pm
    jmp read_loop


cambiar_lm:

    ;; Vamos a desactivar la paginación de 32 bits
    mov eax, cr0
    and eax, 01111111111111111111111111111111b
    mov cr0, eax

    ;; Limpiamos las tablas de paginación
    mov edi, 0x1000
    mov cr3, edi
    xor eax, eax
    mov ecx, 4096
    rep stosd
    mov edi, cr3

    ;; Colocamos las nuevas tablas de 64 bits
    mov DWORD [edi], 0x2003
    add edi, 0x1000
    mov DWORD [edi], 0x3003
    add edi, 0x1000
    mov DWORD [edi], 0x4003
    add edi, 0x1000
    mov ebx, 0x00000003
    mov ecx, 512

    switch_to_lm_set_entry:
    mov DWORD [edi], ebx
    add ebx, 0x1000
    add edi, 8
    loop switch_to_lm_set_entry

    ;; Ahora tenemos que activar la nueva paginación extendida
    mov eax, cr4
    or eax, 1 << 5
    mov cr4, eax

    ;; Ponemos el bit LM
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8
    wrmsr

    ;; Activamos la paginación
    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax

    ;; Cargamos la nueva GDT de 64 bits
    lgdt [gdt_descriptor]
    call iniciar_lm
    ret

[bits 64]

iniciar_lm:
    ;; Actualizamos los registros de segmento
    mov ax, DATA_SEG
    mov ds, ax
    mov ss, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    ;; Actualizamos los punteros base y de pila
    mov ebp, 0x90000
    mov esp, ebp

    ;; Bienvenido a modo 64 bits

    call lm_main
