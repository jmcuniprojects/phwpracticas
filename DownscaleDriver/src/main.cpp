#include <iostream>
#include <vector>
#include <chrono>
#include <fstream>
#include <jpgd.cpp>
#include <toojpeg.h>
#include <lodepng.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#define Funcion1 _IOW('a','a',int*)
#define Funcion2 _IOW('a','b',int*)

std::ofstream* myFile;

void fileOutput(unsigned char byte)
{
  myFile->put(byte);
}

int main ( int argc, char *argv[] ){
	//Creaci'on de variables necesarias 
	int width = 0;
	int height = 0;
	int actual = 0;
	int req = 3; //> Canales en los que queremos la imagen, en este caso RGB
	if(argc < 2){
		std::cout << "Especifique una imagen por favor" << "\n";
		return 0;
	}
	if (argv[1] == nullptr)
	{
		std::cout << "Ruta incorrecta" << "\n";
		return 0;
	}
	std::string fil(argv[1]);//> Nombre del fichero, guardado para su posterior uso al escribir
	std::string imagenTxtR = "";//> String que contendra el canal R

	//Descompresion de la imagen jpg
	unsigned char* image = jpgd::decompress_jpeg_image_from_file(fil.c_str(),&width,&height,&actual,req);
	int tam = width*height;
	imagenTxtR += std::to_string(width)+ ' ';
	imagenTxtR += std::to_string(height) + ' ';
	std::string imagenTxtG = imagenTxtR;//> String que contendra el canal G
	std::string imagenTxtB = imagenTxtR;//> String que contendra el canal B
	std::vector<int> imagenR(tam);
	std::vector<int> imagenG(tam);
	std::vector<int> imagenB(tam);
	//Casteo de los valores devueltos a las variantes necesarias
	for (int i = 0; i < width*height*3; i+=3)
	{
		int aux = (int)image[i];
		imagenTxtR += std::to_string(aux) + ' ';
		imagenR.push_back(aux);
		aux = (int)image[i+1];
		imagenTxtG += std::to_string(aux) + ' ';
		imagenG.push_back(aux);
		aux = (int)image[i+2];
		imagenTxtB += std::to_string(aux) + ' ';
		imagenB.push_back(aux);
	}

	//Apertura del driver
	int fd = open("/dev/DSDriver0", O_RDWR);
	if(fd < 0){
		std::cout << "El driver no esta cargado\n";
		return 0;
	}

	//Procesamos cada canal independientemente con una llamada cada vez
	ssize_t pos = 0;
	ssize_t ret = 0;
	auto inicio = std::chrono::high_resolution_clock::now();
	while(pos != imagenTxtR.size()){
		ret = write(fd, imagenTxtR.c_str()+pos, imagenTxtR.size());
		if(ret == -14){
			std::cout << "Error escribiendo imagen" << "\n";
			return 0;
		}
		pos+=ret;
	}
	auto fin = std::chrono::high_resolution_clock::now();
	auto writeTime = std::chrono::duration_cast<std::chrono::nanoseconds>(fin - inicio);
	unsigned char imagenMiniR[height/2*width/2];
	pos = 0;
	ssize_t last = 1;
	while(last != 0){
		last = read(fd,imagenMiniR+pos,imagenTxtR.size());
		pos += last;
	}

	pos = 0;
	while(pos != imagenTxtG.size())
		pos += write(fd, imagenTxtG.c_str()+pos, imagenTxtG.size());
	unsigned char imagenMiniG[height/2*width/2];
	pos = 0;
	last = 1;
	while(last != 0){
		last = read(fd,imagenMiniG+pos,imagenTxtG.size());
		pos += last;
	}

	pos = 0;
	while(pos != imagenTxtB.size())
		pos += write(fd, imagenTxtB.c_str()+pos, imagenTxtB.size());
	unsigned char imagenMiniB[height/2*width/2];
	pos = 0;
	last = 1;
	while(last != 0){
		last = read(fd,imagenMiniB+pos,imagenTxtB.size());
		pos += last;
	}

	std::cout << "Funciones de driver terminadas | Usando write" << "\n" <<
	 "Tiempo transcurrido: " << (writeTime.count()*3) << " Nanosegundos\n";

	//Unimos los tres canales reescalados
	unsigned char imagenMiniFin[(height/2*width/2)*3];
	int n = 0;
	for (int i = 0; i < (height/2)*(width/2)*3; i+=3)
	{
		imagenMiniFin[i] = imagenMiniR[n];
		imagenMiniFin[i+1] = imagenMiniG[n];
		imagenMiniFin[i+2] = imagenMiniB[n];
		n++;
	}
	
	std::cout << "Conversion hecha" << "\n\n";

	//Generamos la imagen repitiendo la compresion a la inversa
	size_t lastindex = fil.find_last_of(".");
	std::string filFinal= fil.substr(0, lastindex);
	myFile = new std::ofstream(filFinal + "_Mini_write.jpg", std::ios_base::out | std::ios_base::binary);
	TooJpeg::writeJpeg(fileOutput,imagenMiniFin,width/2,height/2,true);
	myFile->close();
	delete myFile;

	std::cout << "Imagen creada, se usa ahora las funciones ioctl\n";
	
	inicio = std::chrono::high_resolution_clock::now();
	ioctl(fd,Funcion1,&tam);
	ioctl(fd,Funcion2,imagenR.data());
	fin = std::chrono::high_resolution_clock::now();
	
	auto ioctlTime = std::chrono::duration_cast<std::chrono::nanoseconds>(fin - inicio);

	pos = 0;
	last = 1;
	while(last != 0){
		last = read(fd,imagenMiniR+pos,imagenR.size());
		pos += last;
	}

	ioctl(fd,Funcion1,&tam);
	ioctl(fd,Funcion2,imagenG.data());
	pos = 0;
	last = 1;
	while(last != 0){
		last = read(fd,imagenMiniG+pos,imagenG.size());
		pos += last;
	}

	ioctl(fd,Funcion1,&tam);
	ioctl(fd,Funcion2,imagenB.data());
	pos = 0;
	last = 1;
	while(last != 0){
		last = read(fd,imagenMiniB+pos,imagenB.size());
		pos += last;
	}

	std::cout << "Funciones del driver completadas | Usando ioctl\n" <<
	 "Tiempo transcurrido: " << (ioctlTime.count()*3) << " Nanosegundos\n";

	n = 0;
	for (int i = 0; i < (height/2)*(width/2)*3; i+=3)
	{
		imagenMiniFin[i] = imagenMiniR[n];
		imagenMiniFin[i+1] = imagenMiniG[n];
		imagenMiniFin[i+2] = imagenMiniB[n];
		n++;
	}
	
	std::cout << "Conversion hecha" << "\n\n";

	myFile = new std::ofstream(filFinal + "_Mini_ioctl.jpg", std::ios_base::out | std::ios_base::binary);
	TooJpeg::writeJpeg(fileOutput,imagenMiniFin,width/2,height/2,true);
	myFile->close();
	delete myFile;

	return 0;
}
