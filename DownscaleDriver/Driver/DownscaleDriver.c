/*
    DownscaleDriver.c
*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/ioctl.h>

#define DRIVER_NAME	  "DSDriver"
#define DRIVER_CLASS  "DSDClass"
#define NUM_DEVICES	  1  /* Número de dispositivos a crear */
#define Funcion1 _IOW('a','a',int*) // Funcion para asignar el tamano a escribir
#define Funcion2 _IOW('a','b',int*) // Funcion para introducir el array de enteros

static dev_t major_minor = -1;
static struct cdev DSDcdev[NUM_DEVICES];
static struct class *DSDclass = NULL;

static char* RGB;
int* RGBint;
static char* returnVec;
static int size;
int width;
int height;
int fila;
int actPos;
int firstTime;
/* ============ Funciones auxiliares del controlador ===================== */

//---------------------- Procesamiento Entero ------------------------//
static void ProcesadoEntero(void){
    int cont = 2;
    int destVec[height][width];
    int i;
    int j;
    for (i = 0; i < height; i++)
    {
        for (j = 0; j < width; j++)
        {
            destVec[i][j] = RGBint[cont];
            cont++;
        }
    }

    actPos = 0;
    kfree(RGBint);
    RGBint = NULL;
    int tam = (height/2)*(width/2);
    returnVec = kmalloc(tam, GFP_KERNEL);
    for (i = 0; i < height; i++)
    {
        if(i+1 < height)
            for (j = 0; j < width; j++)
            {
                if(j+1 < width)
                    if(actPos <= tam){
                        int number = (destVec[i][j]+destVec[i][j+1]+destVec[i+1][j]+destVec[i+1][j+1])/4;
                        returnVec[actPos] = (unsigned char)number;
                        actPos++;
                    }
            }
    }
}
//---------------------- Reset de variables -------------------------//
static void reset(void){
    kfree(RGB);
    RGB = NULL;
    kfree(RGBint);
    RGBint = NULL;
    kfree(returnVec);
    returnVec = NULL;
    size = 0;
    width = 0;
    height = 0;
    fila = 0;
    actPos = 0;
    firstTime = 0;
} 

/* ============ Funciones que implementan las operaciones del controlador ============= */

static int DSDopen(struct inode *inode, struct file *file) {
    int minor;
    minor = iminor(inode);
    pr_info("DSDriver: DSDopen%i",minor);
    return 0;
}

static ssize_t DSDread(struct file *file, char __user *buffer, size_t count, loff_t *f_pos) {
    if (actPos != 0)
    {
        ssize_t tam = (2*width*4);
        if(tam > actPos){
            tam = actPos;
            actPos = 0;
        } else
            actPos = actPos - (int)tam;
        if(copy_to_user(buffer, returnVec, tam))
            return -EFAULT;

        if(actPos <= 0){
            pr_info("DSDriver: Lectura terminada");
            reset();
        }
        else
            returnVec = returnVec+tam;
        return tam;
    }
    return 0;
}

static ssize_t DSDwrite(struct file *file, const char __user *buffer, size_t count, loff_t *f_pos) {
    //Definimos variables
    unsigned int currentPos = 0;
    unsigned int currentNum = 0;
    unsigned int numCount = 0;
    int i = 0;
    int j = 0;
    int pow = 0;
    int k = 0;
    int leido = 0;
    int tam;
    
    //Comprobamos si es el principio del archivo
    if(firstTime == 0){
        if(RGB != NULL)
            kfree(RGB);
        tam = (2*4);
        RGB = kmalloc((2*4)+64,GFP_KERNEL);
        if(copy_from_user(RGB, buffer, (ssize_t)tam)){
            return -EFAULT;
        }
        //Si es el principio leemos altura y anchura
        for (j = 0; j < 2; j++)
        {
            while(RGB[currentPos] != ' ')
            {
                currentNum++;
                currentPos++;
            }

            unsigned int num = 0;
            int localNum = 0;
            for (i = currentPos - currentNum; i < currentPos; i++)
            {
                int position = 1;
                for (pow = 0; pow < currentNum - localNum - 1; pow++)
                {
                    position *= 10;
                }
                num += ((int)(RGB[i] - '0')) * position;
                localNum++;
            }
            if(j == 0)
                width = num;
            else 
                height = num;
            numCount++;
            currentNum = 0;
            currentPos++;
        }
        fila = 0;
        leido += currentPos;
        pr_info("DSDriver: Altura: %i, Anchura %i", height, width);
        //Reservamos la memoria para el vector final
        returnVec = kmalloc((height/2*width/2*4)+128,GFP_KERNEL);
        actPos = 0;
        size = (height/2*width/2*4); 

        //Tomamos las siguientes 2 filas tras altura y anchura
        kfree(RGB);
        tam = (2*width*4);
        RGB = kmalloc((2*width*4)+64,GFP_KERNEL);
        if(copy_from_user(RGB, buffer+currentPos, (ssize_t)tam)){
            return -EFAULT;
        }
        currentPos = 0;
    } else {
        //No hay que tener nada mas en cuenta asi que simplemente tomamos lo que toca
        if(RGB != NULL)
            kfree(RGB);
        tam = (2*width*4);
        RGB = kmalloc((2*width*4)+64,GFP_KERNEL);
        if(copy_from_user(RGB, buffer, tam)){
            return -EFAULT;
        }
    }
    
    //Ahora partimos de que si sabemos altura y anchura, por tanto leemos 2 filas
    int destVec[2][width];
    int localFila = 0;
    for (i = 0; i < 2; i++)
    {
        if(fila+1 > height)
            break;
        for (j = 0; j < width; j++)
        {
            //----------------------
            while(currentPos < (int)tam){
                if(RGB[currentPos] == ' ' || currentNum == 3)
                    break;
                currentNum++;
                currentPos++;
            }

            if(currentPos < (int)tam){
                unsigned int num = 0;
                int localNum = 0;
                for (k = currentPos - currentNum; k < currentPos; k++)
                {
                    int position = 1;
                    for (pow = 0; pow < currentNum - localNum - 1; pow++)
                    {
                        position *= 10;
                    }
                    num += ((int)(RGB[k] - '0')) * position;
                    localNum++;
                }
                destVec[i][j] = num;
                //pr_info("DSDriver: Numero leido %i", destVec[i][j]);
                numCount++;
                currentNum = 0;
                currentPos++;
            } else {
                pr_info("DSDriver: Out of memory, Coodenadas %i | %i, CurrentNum %i", fila, j, currentNum);
                reset();
                return -EFAULT;
            }
            //----------------------
        }
        fila++;
        localFila++;
    }

    if(localFila == 2)
        for (j = 0; j < width; j+=2)
        {
            if(j+1<width){
                int number = (destVec[0][j]+destVec[0][j+1]+destVec[1][j]+destVec[1][j+1])/4;
                if(number < 0 || number > 255){
                    pr_info("DSDriver: Numero %i", number);
                    pr_info("DSDriver: Numero incorrecto, Coodenadas %i | %i", fila, j);
                    reset();
                    return -EFAULT;
                }

                //Conversion a byte
                returnVec[actPos] = (unsigned char)number;
                actPos++;
            }
        }

    leido += currentPos;
    firstTime += leido;
    kfree(RGB);
    RGB = NULL;
    return leido;
}

static long DSDioctl(struct file *file, unsigned int cmd, unsigned long arg){
    switch (cmd)
    {
    case Funcion1:
        pr_info("DSDriver: Funcion1");
        if (copy_from_user(&size,(int*)arg,sizeof(size)))
        {
            return -EFAULT;
        }
        pr_info("DSDriver: Fin Funcion1 %i", size);
        break;
    case Funcion2:
        pr_info("DSDriver: Funcion2");
        RGBint = kmalloc(sizeof(int)*(size+64), GFP_KERNEL);
        if (copy_from_user(RGBint,(int*)arg,sizeof(int)*size))
        {
            return -EFAULT;
        }
        width = RGBint[0];
        height = RGBint[1];
        ProcesadoEntero();
        pr_info("DSDriver: Fin Funcion2");
        break;
    default:
        break;
    }
    return 0;
}

static int DSDrelease(struct inode *inode, struct file *file) {
    pr_info("DSDriver: DSDrelease");
    return 0;
}

/* ============ Estructura con las operaciones del controlador ============= */

static const struct file_operations DSD_fops = {
    .owner = THIS_MODULE,
    .open = DSDopen,
    .read = DSDread,
    .write = DSDwrite,
    .unlocked_ioctl = DSDioctl,
    .release = DSDrelease,
};

/* ============ Inicialización del controlador ============= */

static int DSDdev_uevent(struct device *dev, struct kobj_uevent_env *env) {
    add_uevent_var(env, "DEVMODE=%#o", 0666);
    return 0;
}

static int __init init_driver(void) {
    int n_device;
    dev_t id_device;
    actPos = 0;
    size = 0;
    width = 0;
    height = 0;
    firstTime = 0;

    if (alloc_chrdev_region(&major_minor, 0, NUM_DEVICES, DRIVER_NAME) < 0) {
        pr_err("DSDriver: Major number assignment failed");
        goto error;
    }

    pr_info("DSDriver: %s driver assigned %d major number\n", DRIVER_NAME, MAJOR(major_minor));

    if((DSDclass = class_create(THIS_MODULE, DRIVER_CLASS)) == NULL) {
        pr_err("DSDriver: Class device registering failed");
        goto error;
    } else
        DSDclass->dev_uevent = DSDdev_uevent;

    pr_info("DSDriver: /sys/class/%s class driver registered\n", DRIVER_CLASS);

    for (n_device = 0; n_device < NUM_DEVICES; n_device++) {
        cdev_init(&DSDcdev[n_device], &DSD_fops);

        id_device = MKDEV(MAJOR(major_minor), MINOR(major_minor) + n_device);
        if(cdev_add(&DSDcdev[n_device], id_device, 1) == -1) {
            pr_err("DSDriver: Device node creation failed");
            goto error;
        }

        if(device_create(DSDclass, NULL, id_device, NULL, DRIVER_NAME "%d", n_device) == NULL) {
            pr_err("DSDriver: Device node creation failed");
            goto error;
        }

        pr_info("DSDriver: Device node /dev/%s%d created\n", DRIVER_NAME, n_device);
    }

    pr_info("DSDriver: DSD driver initialized and loaded\n");
    return 0;

error:
    if(DSDclass)
        class_destroy(DSDclass);

    if(major_minor != -1)
        unregister_chrdev_region(major_minor, NUM_DEVICES);

    return -1;
}

/* ============ Descarga del controlador ============= */

static void __exit exit_driver(void) {
    int n_device;

    for (n_device = 0; n_device < NUM_DEVICES; n_device++) {
        device_destroy(DSDclass, MKDEV(MAJOR(major_minor), MINOR(major_minor) + n_device));
        cdev_del(&DSDcdev[n_device]);
    }

    class_destroy(DSDclass);

    unregister_chrdev_region(major_minor, NUM_DEVICES);

    pr_info("DSDriver: DSD driver unloaded\n");
}

/* ============ Meta-datos ============= */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jorge Martinez y Alberto Rodriguez");
MODULE_VERSION("1.0");
MODULE_DESCRIPTION("Downscale and gray transformations");

module_init(init_driver)
module_exit(exit_driver)
