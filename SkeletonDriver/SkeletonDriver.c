#include<linux/module.h> /* printk y otras definiciones */
#include<linux/kernel.h> /* constantes KERN_XX */


static int __init init_driver(void) 
{
    printk(KERN_INFO "Skeleton driver loaded\n");
    return 0;
}

static void __exit exit_driver(void) 
{
    printk(KERN_INFO "Skeleton driver unloaded\n");
}


MODULE_LICENSE("GPL"); /* Obligatorio */
module_init(init_driver);
module_exit(exit_driver);